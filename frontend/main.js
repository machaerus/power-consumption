var APIURL = "http://localhost:5000"

$(document).ready(function() {

	function load(variable, start, end) {
		$.ajax({
			type: "POST",
			async: true,
			dataType: "json",
			contentType: "application/json; charset=utf-8",
			url: APIURL,
			data: JSON.stringify({
				interval: {
					variable: variable,
					start: start,
					end: end
				}
			}),
			success: function(d, textStatus, xhr) {
				data = d;
				
				UIkit.notification({
					message: "Data loaded!",
					status: 'success',
					pos: 'top-right',
					timeout: 2000
				});

				updateGraph(variable, data);

			},
			error: function(xhr, textStatus, error) {
				UIkit.notification({
					message: "Something went wrong!",
					status: 'danger',
					pos: 'top-right',
					timeout: 2000
				});
				console.log(xhr.statusCode());
				console.log(error);
				console.log(textStatus);
			}
		});
	}

	function updateGraph(variable, data) {

		// console.log(data);
		// console.log(data.data[0])

		var timeseries = [];

		for ( var i = 0; i < data.data.length; i++) {
			var e = data.data[i];
			timeseries.push({
				date: new Date(e['date']),
				value: e[variable]
			})
		}

		console.log("Timeseries:")
		console.log(timeseries)

		var chart = d3_timeseries()
			.addSerie(timeseries, 
				{ x: 'date', y: 'value' },
				{ interpolate: 'monotone', color: "orange", width: 1.5 }
			)
			.width(1100);

		$('#graphTitle').text(variable);
		$('#chart').html("");
		chart('#chart');

	}

	function update() {
		var start_year = $("#startYear").val();
		var start_month = $("#startMonth").val();
		var end_year = $("#endYear").val();
		var end_month = $("#endMonth").val();
		var variable = $("#variable").val();
		load(
			variable,
			start_year + "-" + start_month, 
			end_year + "-" + end_month,
		);
	}

	$(".updatable").change(function() {
		update();
	});

	update();

});
