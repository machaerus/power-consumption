#!/usr/bin/env python
from flask import Flask, request
from flask_json import FlaskJSON, JsonError, json_response, as_json
from flask_cors import CORS
import psycopg2
from dateparser import parse
import pandas as pd

application = Flask(__name__)
FlaskJSON(application)
CORS(application)

# this of course shouldn't be hardcoded, but rather
# stored as env variables – just a demo
USER = "postgres"
PASS = "R6Ce#sCurYbbN!Kvc7@u"
DB = "power_consumption"
HOST = "127.0.0.1"
PORT = "5432"

columns = [
'date','Global_active_power', 'Global_reactive_power', 'Voltage',
'Global_intensity', 'Sub_metering_1', 'Sub_metering_2',
'Sub_metering_3']

def fetch_data(variable, start, end):
	"""
	This method will fetch any arbitrary time frame, bin it and return.
	For the purpose of this demo, data in the database is already 
	aggregated by hour, so we have the total of 34589 records:
	not a big deal. But normally the responses should be paginated.
	"""
	
	dt_start = str(parse(start).strftime("%Y-%m-%d %H:%M:%S"))
	dt_end = str(parse(end).strftime("%Y-%m-%d %H:%M:%S"))
	
	print("Fetching data from {} to {}".format(dt_start, dt_end))

	con = psycopg2.connect(dbname=DB, host=HOST, user=USER, password=PASS)
	con.autocommit = True
	cur = con.cursor()
	
	cur.execute("""
		SELECT * FROM household 
		WHERE 
			date > TO_TIMESTAMP(%s, 'YYYY-MM-DD HH24:MI:SS')
			AND date < TO_TIMESTAMP(%s, 'YYYY-MM-DD HH24:MI:SS')
		;""", (dt_start, dt_end))
	
	res = cur.fetchall()
	
	con.close()
	cur.close()

	# This resampling could be done with SQL.
	# Pandas is actually sometimes faster, though I'm not sure
	# about this case – just used it for convenience, since 
	# performance is not an issue for this demo.
	df = pd.DataFrame(res, columns=columns)
	df = df[['date', variable]]
	# print(df)
	df.set_index('date', inplace=True)
	df = df.resample('D').mean()
	df['date'] = df.index
	
	return df.to_dict(orient='records')

#-----------------------------------------------------------------------

@application.route('/', methods=['POST'])
def get_data():
	application.logger.debug("Calling the data API")
	data = request.get_json(force=True)

	# expected input:
	#
	# data = {
	# 	"interval": {
	#		"variable": "Global_active_power",
	# 		"start": "2009-01-01",
	# 		"end": "2009-01-02"
	# 	}
	# }
	#
	# but actually other date formats will work as well

	application.logger.debug("Input received:")
	application.logger.debug(data)

	try:
		assert "interval" in data.keys()
		assert "variable" in data['interval'].keys()
		assert "start" in data['interval'].keys()
		assert "end" in data['interval'].keys()
	except AssertionError as e:
		print(e)
		print("Invalid value!")
		raise JsonError(description="Invalid value")

	variable = data['interval']['variable']
	start = data['interval']['start']
	end = data['interval']['end']

	try:
		application.logger.debug("Querying the database")
		res = fetch_data(variable, start, end)
		return json_response(data=res)
		
	except Exception as e:
		application.logger.exception("Error fetching data from the database!")
		raise JsonError(description="Error fetching data from the database!")

#-----------------------------------------------------------------------

if __name__ == "__main__":
	application.run(host="localhost", debug=True, use_reloader=True)
