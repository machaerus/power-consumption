# Power consumption data analysis

This repo contains some exploratory analysis of the household power consumption dataset that can be found [here](https://archive.ics.uci.edu/ml/datasets/individual+household+electric+power+consumption), and a small web app to showcase the data in an easy way.

## Dataset analysis.ipynb

Exploratory analysis of the dataset, including visualizing the data, identifying some patterns, and a basic attempt to model them.

## Database setup.ipynb

This notebook was used to create a PostgreSQL database for the dashboard, and to populate it with the data.

## Fetch data from the database.ipynb

A notebook to try out extracting the data from the database in a way the app will do it.

## Backend

A simple Flask app, working as an API to serve the data from the database. Currently the output data will be binned by day – this could be configured to change depending on the time frame requested, or we could give the user direct control over it.

To try the app, install the requirements and run it with:

	python application.py

You can test it with:

	./test.sh

if you have `httpie` installed, otherwise use `curl` or whatever you like.

It is configured to run on localhost.

## Frontend

A simple static frontend for the dashboard app, showcasing the dataset. It used D3 Timeseries for plotting, and just jQuery for the logic. Installation:

	npm install
